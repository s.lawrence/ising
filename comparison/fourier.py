#!/usr/bin/env python

import numpy as np
import sys

dat = np.array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])

decay = max(dat[:,0])/4

@np.vectorize
def fourier(omega):
    return -np.trapz(dat[:,1]*np.sin(omega*dat[:,0])*np.exp(-(dat[:,0]/decay)**2), dat[:,0]) / 2
    #return -np.trapz(dat[:,1]*np.sin(omega*dat[:,0])*np.exp(-dat[:,0]/decay), dat[:,0])

omega = np.linspace(0,30,501)
amp = fourier(omega).real

for v in zip(omega,amp):
    print(*v)

