#!/usr/bin/env python

import sys
import numpy as np
import numpy.random as random

Lx = int(sys.argv[1])
Ly = int(sys.argv[2])
mu = float(sys.argv[3])
J = 1.
beta = float(sys.argv[4])
#beta = 1/float(sys.argv[4])
T = float(sys.argv[5])

pauli_x = np.array([[0,1],[1,0]])
pauli_z = np.array([[1,0],[0,-1]])

sigma_x = [np.eye(1)]*Lx*Ly
sigma_z = [np.eye(1)]*Lx*Ly

for n in range(Lx*Ly):
    for m in range(Lx*Ly):
        if n == m:
            sigma_x[n] = np.kron(sigma_x[n], pauli_x)
            sigma_z[n] = np.kron(sigma_z[n], pauli_z)
        else:
            sigma_x[n] = np.kron(sigma_x[n], np.eye(2))
            sigma_z[n] = np.kron(sigma_z[n], np.eye(2))

    H = np.zeros(sigma_x[0].shape)
    W = np.zeros(sigma_x[0].shape, dtype=np.complex128) # The wave goes in the x-direction.

for x in range(Lx):
    for y in range(Ly):
        n = y*Lx+x
        H -= mu*sigma_x[n]
        W -= mu*sigma_x[n] * np.exp(1j*2*np.pi*x/Lx)
        xp = (x+1)%Lx
        yp = y
        m = yp*Lx+xp
        H -= J*sigma_z[n]@sigma_z[m]
        W -= J*sigma_z[n]@sigma_z[m] * np.exp(1j*2*np.pi*(x+0.5)/Lx)
        xp = x
        yp = (y+1)%Ly
        m = yp*Lx+xp
        H -= J*sigma_z[n]@sigma_z[m]
        W -= J*sigma_z[n]@sigma_z[m] * np.exp(1j*2*np.pi*x/Lx)

vals, vecs = np.linalg.eigh(H)
rho = vecs @ np.diag(np.exp(-beta*vals)) @ vecs.conj().T
rho /= rho.trace()

for time in np.linspace(0,T,10001):
    U = vecs @ np.diag(np.exp(-1j*time*vals)) @ vecs.conj().T

    print(time, (rho @ U.conj().T @ W.conj().T @ U @ W).trace().imag)

