#!/usr/bin/env python

import numpy as np
import subprocess

import matplotlib
import matplotlib.pyplot as plt
#matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

mus = np.linspace(2.5,3.5,21)

def susceptibility_(mu, Lx, Ly, beta, Nbeta, Nbatch):
    print(f'Running: {mu} {Lx} {Ly} {beta} {Nbeta} {Nbatch}')
    K = 20
    args = [str(Lx), str(Ly), str(mu), str(beta), str(Nbeta), str(K), str(Nbatch)]
    proc = subprocess.run(['../thermal/susceptibility', *args], check=True, capture_output=True)
    dat = np.array([[float(x) for x in l.split()] for l in proc.stdout.splitlines()])
    m1, m2 = dat[:,0], dat[:,1]
    chi = m2/Nbeta - Lx*Ly*m1**2
    return np.mean(chi), np.std(chi)/np.sqrt(K)

@np.vectorize
def susceptibility(mu, L, Nbeta, Nbatch):
    return susceptibility_(mu, L, L, L, Nbeta, Nbatch)

plt.figure(figsize=(6,4), dpi=200)
for L in [10,20,30]:
    print(f'{L}')
    chi, sig = susceptibility(mus, L, L*8, 400)
    plt.errorbar(mus, chi, yerr=sig, label=f'$L = {L}$', fmt='.')
plt.legend(loc='best')
plt.tight_layout()
#plt.show()
plt.savefig('figures/susceptibility.png', transparent=True)
