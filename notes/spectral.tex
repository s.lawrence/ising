\documentclass[11pt,letterpaper]{article}
\usepackage{amsmath,amssymb}
\usepackage{xcolor}
\usepackage[margin=1.4in]{geometry}
\usepackage{comment}

\let\Re\relax \let\Im\relax

\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\Im}{Im}

\newcommand{\todo}{\colorbox{pink}{\textsc{Todo}}}

\begin{document}
\title{Spectral Function Conventions}
\author{Scott Lawrence}
\maketitle

\paragraph{What we want:} Let $H_0 = \int\mathcal H_0$ be the Hamiltonian of the Ising model. Given a time-dependent perturbed Hamiltonian
\[
H_\epsilon(t) = H_0 + \epsilon \delta(t) \mathcal O
\text,
\]
we would like to compute the real-time response $\langle \mathcal O(t)\rangle$. In general this is a thoroughly nonequilibrium question. However, the limiting behavior for small $\epsilon$ can derived from correlators evaluated in equilibrium.
\[
\frac{d}{d\epsilon}\langle \mathcal O(t)\rangle =
\frac{d}{d\epsilon} \big\langle e^{i \epsilon \mathcal O} e^{i H_0 t} \mathcal O  e^{-i H_0 t} e^{-i \epsilon \mathcal O}\big\rangle
=
-i \langle [\mathcal O(t), \mathcal O(0)] \rangle
\]
Note that because $\mathcal O(t)$ is Hermitian, we have $\langle \mathcal O(t) \mathcal O(0)\rangle^\dagger = \langle \mathcal O(0) \mathcal O(t)\rangle$. The commutator can therefore be expressed in terms of just one correlation function, and it follows that
\[
\langle \mathcal O(t)\rangle = 2 \epsilon \Im \langle \mathcal O(t) \mathcal O(0)\rangle + O(\epsilon^2)
\text.
\]

\paragraph{What we have:} Lattice Monte Carlo yields the Euclidean correlator, defined as
\[
C(\tau) = Z^{-1} \Tr e^{-(\beta-\tau)H_0} \mathcal O e^{-\tau H_0} \mathcal O
\text.
\]

\paragraph{Connecting the correlators:} To connect the two correlators, we expand both as explicit sums over eigenstates of the unperturbed Hamiltonian $H_0$. The imaginary-time correlator is
\[
C(\tau) =
Z^{-1} \sum_{E,E'} e^{-\beta E} e^{-\tau (E'-E)} |\langle E' |  \mathcal O | E \rangle|^2
\text,
\]
and the imaginary part of the real-time correlator is
\[
\Im C(t) =
- Z^{-1}
\sum_{E,E'}
e^{-\beta E}
|\langle E' |  \mathcal O | E \rangle|^2
\sin t (E'-E)
\text.
\]
(In both cases, we have slightly abused notation with the assumption that for every eigenvalue $E$, there is only one corresponding eigenstate.) The two correlators can be related through a pre-spectral function, defined as
\[
\sigma(\omega) = Z^{-1} \sum_E e^{-\beta E} |\langle E+\omega|\mathcal O |E\rangle|^2
\text.
\]
In terms of $\sigma(\cdot)$, we have
\[
C(\tau) = \int_{\mathbb R} d\omega \;\sigma(\omega) e^{-\tau\omega}
\text{\; and \;}
\Im C(t) = -\int_{\mathbb R} d\omega\;\sigma(\omega) \sin(t \omega)
\text.
\]

\paragraph{Symmetry:} The Euclidean correlation function obeys $C(\tau) = C(\beta-\tau)$, but this is not evident when it is expressed in terms of $\sigma(\cdot)$. Where did this symmetry go?

First note that this symmetry has an incarnation in $\sigma(\cdot)$ itself:
\[
\sigma(-\omega)
=
Z^{-1}\sum_E e^{-\beta (E+\omega)} |\langle E|\mathcal O|E+\omega\rangle|^2
= e^{-\beta\omega} \sigma(\omega)
\text.
\]
The symmetry of $C(\tau)$ follows from this identity:
\[
C(\beta-\tau)
=
\int_{\mathbb R} d\omega\; \sigma(-\omega)e^{\beta\omega}e^{-\tau\omega}
=\int_{\mathbb R} d \omega\; \sigma(\omega) e^{-\tau\omega} = C(\tau)
\text.
\]

\paragraph{The spectral function:}
The use of $\sigma(\cdot)$ is a little unnatural, first because its domain is $\mathbb R$ instead of the more traditional $\mathbb R^+$, and second because the $\tau \rightarrow \beta-\tau$ symmetry is not immediately evident from the formula for $C(\tau)$. These two can both be fixed by switching out $\sigma(\cdot)$ for the spectral function $\rho(\cdot)$. The spectral function is usually defined as the Fourier transform of the real-time correlator:
\[
\rho(\omega) = -\frac {2} {\pi} \int_0^{\infty} d t\; \Im C(t) \sin \omega t
\text.
\]
It remains to relate the spectral function to the pre-spectral function $\sigma(\cdot)$ and the imaginary-time correlator. Either task is achieved simply by evaluating the Fourier transform, using an appropriate expression for $C(t)$ from above. In terms of the pre-spectral function, the spectral function is given by
\[
\rho(\omega) = \frac 1 2 \left[\sigma(\omega) - \sigma(-\omega)\right]
\text.
\]
The Euclidean correlator itself is now seen to be equal to
\[
C(\tau) =
2 \int_{\mathbb R^+} d\omega\;\rho(\omega) \frac{\cosh \omega\big(\tau - \frac{\beta\omega}2\big)}{\sinh \frac{\beta\omega}{2}}
\text.
\]
This matches the usual convention in the lattice QCD literature\footnote{See for example \texttt{arXiv:1801.10348}.}. Do not forget that both $\rho(\cdot)$ and $\sigma(\cdot)$ are, despite the traditional notation, implicitly functions of both the temperature and the chosen operator.

\end{document}

