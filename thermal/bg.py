#!/usr/bin/env python

# Backus-Gilbert reconstruction of the spectral function.

import numpy as np
import sys


########
# INPUT

dat = np.array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])
cor, err = dat[:,0], dat[:,1]
beta = len(cor)


################
# EXPERIMENTING

# The integration kernel.
def K(tau,omega):
    return np.cosh(omega*(tau-beta/2))


# Pick rho_i
Omega = np.linspace(0,1,1000)
Q = np.zeros((beta,1000))

for i in range(beta):
    Q[i] = np.exp(-(Omega-i)**2 / 2)

def q(i,omega):
    return np.interp(omega, Omega, Q[i,:])


def delta(omega, omegap):
    return np.sum(np.fromiter((q(i,omegap)*K(i,omega) for i in np.arange(beta)), dtype=np.double))
    #return np.sum(np.interp(omegap, q[:]*K(tau,omega))


import matplotlib.pyplot as plt

# Plot spectral function.

# Plot delta function
plt.plot(Omega, np.vectorize(lambda w: delta(w, 2.))(Omega))

plt.show()

