! Continuous Wolff Algorithm.
!
! As described by Bloete and Deng (2002).

program cwa

    real, parameter :: PI = 3.14159265358979323846

    character(len=50) :: Lxstr, Lystr, mustr, betastr, Nsamplestr
    integer :: Nsample
    integer :: Lx, Ly
    real :: mu, beta

    integer :: maxiface = 10
    logical, allocatable :: spins(:,:)
    integer, allocatable :: niface(:,:)
    real, allocatable :: ifaces(:,:,:)

    if (command_argument_count() /= 5) then
        stop "Lx Ly mu beta Nsample"
    end if

    call get_command_argument(1, Lxstr)
    call get_command_argument(2, Lystr)
    call get_command_argument(3, mustr)
    call get_command_argument(4, betastr)
    call get_command_argument(5, Nsamplestr)

    read (Lxstr,*) Lx
    read (Lystr,*) Ly
    read (mustr,*) mu
    read (betastr,*) beta
    read (Nsamplestr,*) Nsample

    allocate(spins(Lx,Ly))
    allocate(niface(Lx,Ly))
    allocate(ifaces(maxiface,Lx,Ly))

    niface = 0
    spins = .false.

    call main

    deallocate(spins)
    deallocate(niface)
    deallocate(ifaces)

contains

subroutine main
    integer :: Ncluster = 4
    integer :: sample, cluster

    do sample = 1,Nsample
        do cluster = 1,Ncluster
            call update()
        end do
        call measure()
    end do
end subroutine

function spin(x,y,t) result(s)
    integer, intent(in) :: x,y
    real, intent(in) :: t
    logical :: s
    integer :: i
    s = spins(x,y)
    do i = 1,niface(x,y)
        if (ifaces(i,x,y) < t) then
            s = .not. s
        else
            exit
        end if
    end do
end function

subroutine update()
    integer :: x, y
    real :: t
    logical :: s

    ! Choose a random site and time to start.
    call random_site(x,y)
    call random_number(t)
    t = t*beta

    ! Determine the spin.
    s = spin(x,y,t)

    ! Form cluster
    ! TODO

    ! Pick new spin
    ! TODO

    ! Create/annihilate interfaces
    ! TODO
end subroutine

subroutine measure()
    print *, 0
end subroutine

subroutine random_site(x,y)
    integer, intent(out) :: x,y
    real :: u
    call random_number(u)
    x = floor(u*Lx)
    call random_number(u)
    y = floor(u*Ly)
end subroutine

end program
