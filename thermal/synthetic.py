#!/usr/bin/env python

# Make synthetic data for testing B-G and fits.

import numpy as np
import sys

beta = float(sys.argv[1])
Nbeta = int(sys.argv[2])
sigma = float(sys.argv[3])

# The integration kernel.
def K(tau,omega):
    return np.cosh(omega*(beta/2-tau)) * np.exp(-omega*beta/2)

@np.vectorize
def rho_plus(omega):
    gamma = 0.1
    omega0 = 2.5
    pure = rho_pure(omega)
    return pure + 1/(np.pi * gamma) * gamma**2 / ((omega-2*omega0)**2+gamma**2)

@np.vectorize
def rho_pure(omega):
    gamma = 0.1
    omega0 = 1.
    return 1/(np.pi * gamma) * gamma**2 / ((omega-omega0)**2+gamma**2)

rho = rho_plus

def C(tau):
    omega = np.linspace(0,5,10000)
    return np.trapz(K(tau,omega)*rho(omega), omega)

for ntau in range(Nbeta):
    tau = ntau/Nbeta*beta
    print(tau, C(tau)+np.random.normal()*sigma, sigma)

