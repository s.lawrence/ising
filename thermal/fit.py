#!/usr/bin/env python

# Extracting the spectral function with ordinary fitting.

from functools import partial
import numpy as np
from scipy.optimize import curve_fit
import sys

def ff_cosh(t, p0, p1, p2):
    return np.cosh(p0*(t-p1))*p2

@partial(np.vectorize, excluded={1,2,3,4})
def ff_bump(t, omega_r, omega_i, amp):
    omega = np.linspace(0,10,10000)
    rho = amp / (np.pi*omega_i) * omega_i**2 / ((omega-omega_r)**2 + omega_i**2)
    rho *= np.exp(-beta/2*omega)
    return np.trapz(np.cosh(omega*(beta/2 - t))*rho,omega)

if False:
    import matplotlib.pyplot as plt
    t = np.linspace(0,10,1000)
    #plt.plot(t, ff_bump(t, 1, 1, 1, 1))
    #plt.plot(t, ff_bump(t, 1, 0.2, 1, 1))
    plt.plot(t, ff_bump(t, 1, 0.01, 1, 1))
    plt.show()
    sys.exit(0)

dat = np.array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])

tau, cor = dat[:,0], dat[:,1]
beta = dat[-1,0]+dat[1,0]

f = ff_bump

skip = int(sys.argv[1])

fitp, fite = curve_fit(f, tau[1+skip:-skip], cor[1+skip:-skip], p0=(1,0.3,1))
print(fitp)

import matplotlib.pyplot as plt

#plt.errorbar(tau[1:], cor[1:], yerr=err[1:])
plt.scatter(tau[1:], cor[1:])
plt.plot(tau[1:], f(tau[1:], *fitp))
plt.show()

