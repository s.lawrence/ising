#!/usr/bin/env python

from mpmath import *
from numpy import vectorize
import sys
import numpy as np

# TODO sane argument parsing
# TODO add smallness of coefficients to cost function

###########
# SETTINGS

mp.dps = 40

beta = mpf(10.)
Nbeta = 30
omegabar = mpf(3.)
sigma = 1.0
lamda = float(sys.argv[1])

VERBOSE = False

#########
# SOLVNG

def K(omega, tau):
    """ Kernel of integration to go from spectral function to Euclidean
    correlator. """
    return exp(-omega*beta/2) * cosh(omega*(tau-beta/2))

def delta_ideal(omega):
    """ Ideal delta function. """
    return exp(-(omega-omegabar)**2/(2*sigma**2)) / (sigma * sqrt(2*pi))


omega = linspace(0,20,100)
b = vectorize(delta_ideal)(omega)
bp = np.ones(Nbeta//2-1)
b = np.concatenate((b,bp))

A = matrix(len(omega)+Nbeta//2-1,Nbeta//2-1)
for itau in range(1,Nbeta//2):
    for iomega in range(len(omega)):
        A[iomega,itau-1] = K(omega[iomega],beta*itau/Nbeta)

for iomega in range(len(omega), len(omega)+Nbeta//2-1):
    A[iomega, iomega-len(omega)] = lamda

# qr_solve(A, b), instead of lu_solve(), purportedly more accurate
q, res = qr_solve(A, b)
if VERBOSE:
    print(f'Solved with residue {res}')


#########
# OUTPUT

def delta(omega):
    r = mpf(0)
    for itau in range(1,Nbeta//2):
        r += K(omega,beta*itau/Nbeta) * q[itau-1]
    return r

# Print the delta function.
for omega in linspace(0,10,101):
    print(omega, delta(omega))

if False:
    # Check the delta function's normalization.
    print(quad(delta,[0,100]))

if False:
    # Print the coefficients.
    for itau in range(1,Nbeta//2):
        print(itau, q[itau-1])



