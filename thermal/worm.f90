program worm

    real, parameter :: PI = 3.14159265358979323846

    character(len=50) :: Lxstr, Lystr, mustr, betastr, Nsamplestr
    integer :: Nsample
    integer :: Lx, Ly
    real :: mu, beta

    if (command_argument_count() /= 5) then
        stop "Lx Ly mu beta Nsample"
    end if

    call get_command_argument(1, Lxstr)
    call get_command_argument(2, Lystr)
    call get_command_argument(3, mustr)
    call get_command_argument(4, betastr)
    call get_command_argument(5, Nsamplestr)

    read (Lxstr,*) Lx
    read (Lystr,*) Ly
    read (mustr,*) mu
    read (betastr,*) beta
    read (Nsamplestr,*) Nsample

contains

end program
