! A one-spin demonstration of CWA.

program spin

    real, parameter :: PI = 3.14159265358979323846

    character(len=50) :: mustr, betastr, Nsamplestr
    integer :: Nsample
    real :: mu, beta

    integer :: maxiface = 100
    logical :: spin0
    integer :: niface = 0
    real, allocatable :: ifaces(:)

    if (command_argument_count() /= 3) then
        stop "mu beta Nsample"
    end if

    call get_command_argument(1, mustr)
    call get_command_argument(2, betastr)
    call get_command_argument(3, Nsamplestr)

    read (mustr,*) mu
    read (betastr,*) beta
    read (Nsamplestr,*) Nsample

    allocate(ifaces(maxiface))

    call main

    deallocate(ifaces)

contains

subroutine main
    integer :: Ncluster = 4
    integer :: sample, cluster

    do sample = 1,Nsample
        do cluster = 1,Ncluster
            call update()
        end do
        call measure()
    end do
end subroutine

subroutine update
    integer :: i
    real :: t, r, l
    logical :: s

    ! Choose a random time to start.
    call random_number(t)
    t = t*beta

    ! Determine the spin.
    s = spin0
    do i = 1,niface
        if (ifaces(i) < t) then
            s = .not. s
        end if
    end do

    ! Form cluster. First, the left edge.
    call random_number(r)
    l = -log(r)
    ! TODO
    ! Next, the right edge.
    call random_number(r)
    l = -log(r)
    ! TODO

    ! Create/annihilate interfaces
    ! TODO
end subroutine

subroutine measure
end subroutine

end program
