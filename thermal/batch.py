#!/usr/bin/env python

import numpy as np
import sys

N = int(sys.argv[1])

def data(l):
    return np.array([float(x) for x in l.split()])

# Read one line to get size.
first = data(sys.stdin.readline())
K = len(first)

dat = np.zeros((N,K))
dat[0,:] = first
n = 1

# Now read the rest.
for line in sys.stdin:
    dat[n,:] = data(line)
    n += 1
    if n >= N:
        print(*np.mean(dat,axis=0))
        n = 0

if n > 0:
    print(*np.mean(dat[:n],axis=0))
