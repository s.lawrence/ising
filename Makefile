FFLAGS = -O3
#FFLAGS = -O0 -g

FFLAGS += -fimplicit-none
FFLAGS += -fdefault-real-8
#PFLAG = -fprofile-generate=profiles/
#PFLAG = -fprofile-use=profiles/

all: thermal/thermal thermal/cwa thermal/susceptibility rt/rt comparison/iso comparison/metropolis thermal/spin thermal/worm

thermal/cwa: thermal/cwa.f90
	gfortran ${FFLAGS} ${PFLAG} -o $@ $<

thermal/spin: thermal/spin.f90
	gfortran ${FFLAGS} ${PFLAG} -o $@ $<

thermal/worm: thermal/worm.f90
	gfortran ${FFLAGS} ${PFLAG} -o $@ $<

thermal/thermal: thermal/thermal.f90
	gfortran ${FFLAGS} ${PFLAG} -o $@ $<

thermal/susceptibility: thermal/susceptibility.f90
	gfortran ${FFLAGS} ${PFLAG} -o $@ $<

comparison/metropolis: comparison/metropolis.f90
	gfortran ${FFLAGS} -o $@ $<

comparison/iso: comparison/iso.f90
	gfortran ${FFLAGS} -o $@ $<

rt/rt: rt/rt.f90
	gfortran ${FFLAGS} -o $@ $<

clean:
	${RM} thermal/cwa thermal/spin thermal/worm thermal/thermal thermal/susceptibility rt/rt comparison/iso comparison/metropolis lattice.mod
