#!/usr/bin/env python

import numpy as np
import subprocess
import sys

# Number of rounds
K = 3

# Command to run
command = sys.argv[1:]

# Original input
lines = sys.stdin.readlines()
headers = [l for l in lines if l.strip()[0] == '#']
data = [l for l in lines if l.strip()[0] != '#']

outs = []
for k in range(K):
    # Resample data
    s = np.random.choice(range(len(data)), len(data))
    sdata = [data[i] for i in s]
    # Convert to bytes
    bdata = bytes(''.join(headers+sdata), encoding='utf-8')
    proc = subprocess.run(command, input=bdata, capture_output=True)
    sys.stderr.write(proc.stderr.decode('utf-8'))
    outs.append(proc.stdout.decode('utf-8').strip().split('\n'))

# Check that all outputs are the same length
lens = set([len(out) for out in outs])
if len(lens) > 1:
    print('Different lengths of output found')
    sys.exit(1)

# Extract the labels
labels = [line.split(':')[0].strip() for line in outs[0]]

# Check that the labels always agree
for out in outs[1:]:
    labels_ = [line.split(':')[0].strip() for line in outs[0]]
    if labels_ != labels:
        print('Labels do not match')
        sys.exit(1)

# Extract data
dat = np.array([[float(line.split(':')[1]) for line in out] for out in outs])

# Average and output
for n in range(len(labels)):
    avg = np.mean(dat[:,n])
    err = np.std(dat[:,n])
    print(f'{labels[n]} : {avg} {err}')

