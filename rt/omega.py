#!/usr/bin/env python

import sys
from numpy import *
import numpy.random as random

Lx = int(sys.argv[1])
Ly = int(sys.argv[2])
mu = float(sys.argv[3])
J = 1.
beta = 1/float(sys.argv[4])

pauli_x = array([[0,1],[1,0]])
pauli_z = array([[1,0],[0,-1]])

sigma_x = [eye(1)]*Lx*Ly
sigma_z = [eye(1)]*Lx*Ly

for n in range(Lx*Ly):
    for m in range(Lx*Ly):
        if n == m:
            sigma_x[n] = kron(sigma_x[n], pauli_x)
            sigma_z[n] = kron(sigma_z[n], pauli_z)
        else:
            sigma_x[n] = kron(sigma_x[n], eye(2))
            sigma_z[n] = kron(sigma_z[n], eye(2))

dim = sigma_x[0].shape[0]
H = zeros(sigma_x[0].shape)
W = zeros(sigma_x[0].shape, dtype=complex128) # The wave goes in the x-direction.

for x in range(Lx):
    for y in range(Ly):
        n = y*Lx+x
        H -= mu*sigma_x[n]
        W -= mu*sigma_x[n] * exp(1j*2*pi*x/Lx)
        xp = (x+1)%Lx
        yp = y
        np = yp*Lx+xp
        H -= J*sigma_z[n]@sigma_z[np]
        W -= J*sigma_z[n]@sigma_z[np] * exp(1j*2*pi*(x+0.5)/Lx)
        xp = x
        yp = (y+1)%Ly
        np = yp*Lx+xp
        H -= J*sigma_z[n]@sigma_z[np]
        W -= J*sigma_z[n]@sigma_z[np] * exp(1j*2*pi*x/Lx)

vals, vecs = linalg.eigh(H)
rho = vecs @ diag(exp(-beta*vals)) @ vecs.conj().T
rho /= rho.trace()

if False:
    mults = set()
    for v in vals:
        mults.add(count_nonzero(abs(vals-v)<1e-6))
    print(vals)
    print(mults)
    sys.exit(0)

WH = vecs.conj().T @ W @ vecs

BINS = 5000
SCALE = 400

bins = zeros(BINS)
mids = arange(BINS)/SCALE
for i in range(dim):
    for j in range(dim):
        coef = WH[i,j] * WH[j,i].conj()
        if abs(coef) > 1e-6 and vals[i] > vals[j]:
            loc = int((vals[i]-vals[j])*SCALE)
            if loc < len(bins):
                bins[loc] += coef.real * exp(-vals[i])
            #print(coef, vals[i]-vals[j])

import matplotlib.pyplot as plt
plt.plot(mids, bins)
plt.show()

#U = vecs @ diag(exp(-1j*T*vals)) @ vecs.conj().T
#print((rho @ U.conj().T @ W.conj().T @ U @ W).trace())

